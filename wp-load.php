<?php

namespace FastAjax;

if(!defined('SHORTINIT'))
	define('SHORTINIT', true);

// error_reporting(0);
// ini_set("display_errors", 0);

/*custom header*/
// set headers ----
_nocache_headers();
@ header('X-Robots-Tag: noindex' );
// base on WP send_nosniff_header();
@ header( 'X-Content-Type-Options: nosniff' );
/*custom header end*/

/*
files_include = array('user', 'postmeta', 'postmeta_update', 'post');
 */
function wp_load($files_include = array()){
	include(up_dirname('wp-content', '', 1).'/wp-load.php');

	// Start loading timer.
	timer_start();

	$files_include = (array) $files_include;

	$addictions = array(
		'user' => array(
			'formatting',
			'capabilities',
			'class-wp-roles',
			'class-wp-role',
			'class-wp-user',
			'user',
			'session',
			'meta',
			'general-template',
			'link-template',
			'kses',
			'rest-api',
			'pluggable',
			'pluggable-deprecated',
		),
		'postmeta' => array(
			'meta',
			'post',
			'revision',
			'formatting',
			'class-wp-post',
		),
		'post' => array(
			'class-wp-tax-query',
			'class-wp-meta-query',
			'post',
			'user',
			'pluggable',
			'rest-api',
			'class-wp-user',
			'kses',
			'meta',
			'capabilities',
			'formatting',
			'comment',
			'taxonomy',
			'class-wp-query',
			'class-wp-post',
			'rewrite',
			'class-wp-rewrite',
			//thumbnail
			'post-thumbnail-template',
			'shortcodes',
			'media',
			'query',
		),
	);

	$new_addictions = array();
	foreach ($files_include as $name_addication) {
		$new_addictions = array_merge($new_addictions, $addictions[$name_addication]);
	}

	$new_addictions = array_unique($new_addictions, SORT_STRING);


	foreach ($new_addictions as $addiction) {
		require_once( ABSPATH . WPINC . '/'. $addiction .'.php' );
	}

	require_once( ABSPATH . WPINC . '/default-constants.php' );
	wp_plugin_directory_constants();

	if(in_array('user', $files_include)){
		\wp_cookie_constants(); //Куки пользователя. Для того чтобы получить авторизованного пользователя
	}

	if(in_array('user', $files_include) || in_array('post', $files_include)){
		\wp_plugin_directory_constants(); //Константы для директорий
	}

	if(in_array('post', $files_include)){
		$GLOBALS['wp_rewrite'] = new \WP_Rewrite();
	}


}

/**
 * Поиск и вывод пути по имени директории
 * @param  string $name_dir [Имя директории до которой нужно опустится]
 * @param  string $dir      [Путь с которым нужно работать. По умолчанию __FILE__]
 * @param  integre $descend [Число на которое нужно опустится в найденной директории]
 * @return string           [new_path]
 */
function up_dirname($name_dir, $dir = '', $lvl = 0){
	$this_dir = $dir? $dir : __FILE__;
	$new_dir = explode($name_dir, $this_dir);
	$result = $new_dir[0].$name_dir;
	if($lvl){
		$result = preg_split('#\\\|/#', $result);
		$result = implode('/', array_slice($result, 0, -1 * $lvl));
	}

	return $result;
}

/**
 * Set the headers to prevent caching for the different browsers.
 *
 * Different browsers support different nocache headers, so several
 * headers must be sent so that all of them get the point that no
 * caching should occur.
 * 
 * base on WP nocache_headers() func
 */
function _nocache_headers() {
	// from wp_get_nocache_headers()
	$headers = array(
		'Expires' => 'Wed, 11 Jan 1984 05:00:00 GMT',
		'Cache-Control' => 'no-cache, must-revalidate, max-age=0',
		'Pragma' => 'no-cache',
		//'Last-Modified' => false,
	);

	//unset( $headers['Last-Modified'] );

	// In PHP 5.3+, make sure we are not sending a Last-Modified header.
	if ( function_exists( 'header_remove' ) ) {
		@ header_remove( 'Last-Modified' );
	}else{
		// In PHP 5.2, send an empty Last-Modified header, but only as a
		// last resort to override a header already sent. #WP23021
		foreach ( headers_list() as $header ) {
			if ( 0 === stripos( $header, 'Last-Modified' ) ) {
				$headers['Last-Modified'] = '';
				break;
			}
		}
	}

	foreach ( $headers as $name => $field_value )
		@ header("{$name}: {$field_value}");
}

include('include/functions.php');