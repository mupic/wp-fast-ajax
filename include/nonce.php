<?php

namespace FastAjax;

function hash($data) {
	return hash_hmac('md5', $data, '}{SD(*hsdh8)(*!}');
}

function create_nonce($action = -1) {
	$user_agent = $_SERVER['HTTP_USER_AGENT'];
	$i = nonce_tick();

	return substr( hash( $i . '|' . $action . '|' . $user_agent ), -12, 10 );
}

function nonce_tick() {
	return ceil(time() / ( 60*60*24 / 2 ));
}

function hash_equals( $a, $b ) {
	$a_length = strlen( $a );
	if ( $a_length !== strlen( $b ) ) {
		return false;
	}
	$result = 0;

	// Do not attempt to "optimize" this.
	for ( $i = 0; $i < $a_length; $i++ ) {
		$result |= ord( $a[ $i ] ) ^ ord( $b[ $i ] );
	}

	return $result === 0;
}

function verify_nonce( $nonce, $action = -1 ) {
	$nonce = (string) $nonce;

	if ( empty( $nonce ) ) {
		return false;
	}

	$user_agent = $_SERVER['HTTP_USER_AGENT'];
	$i = nonce_tick();

	// Nonce generated 0-12 hours ago
	$expected = substr( hash( $i . '|' . $action . '|' . $user_agent ), -12, 10 );
	if ( hash_equals( $expected, $nonce ) ) {
		return 1;
	}

	// Nonce generated 12-24 hours ago
	$expected = substr( hash( ( $i - 1 ) . '|' . $action . '|' . $user_agent ), -12, 10 );
	if ( hash_equals( $expected, $nonce ) ) {
		return 2;
	}

	// Invalid nonce
	return false;
}